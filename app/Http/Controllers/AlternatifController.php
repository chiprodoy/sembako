<?php

namespace App\Http\Controllers;

use App\alternatif;
use App\BobotAlternatif;
use App\Http\Resources\BobotAlternatif as BobotAlternatifResource;
use Illuminate\Http\Request;
use App\Http\Resources\Alternatif as AlternatifResource;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class AlternatifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $maxJarak=20;
    public function index()
    {
        //
        $act = Input::get('act');
        $lat = Input::get('latitude');
        $longit= Input::get('longitude');
        $kVariasiProduk=Input::get('variasi_produk');
        $kRata2Harga=Input::get('rata_rata_harga');
        $kProdukKuantitas=Input::get('kuantitas_produk');
        $kJarak=Input::get('jarak');

        switch ($act){
            case "rekomendasi":
               return $this->rekomendasi($kVariasiProduk,$kRata2Harga,$kProdukKuantitas,$kJarak,$lat,$longit);
                break;
            case "nearby" :

              return  AlternatifResource::collection(alternatif::select('id','kode_alternatif',
                    'nama_alternatif','keterangan',
                  'latitude',
                  'longitude',
                  DB::raw("ROUND(111.1111 * DEGREES(ACOS(COS(RADIANS($lat)) * COS(RADIANS(alternatif.latitude)) * COS(RADIANS($longit) - RADIANS(alternatif.longitude)) + SIN(RADIANS($lat)) * SIN(RADIANS(alternatif.latitude)))),2) as jarak"))
                  ->where(DB::raw("ROUND(111.1111 * DEGREES(ACOS(COS(RADIANS($lat)) * COS(RADIANS(alternatif.latitude)) * COS(RADIANS($longit) - RADIANS(alternatif.longitude)) + SIN(RADIANS($lat)) * SIN(RADIANS(alternatif.latitude)))),2)"),'<=',6)
                  ->get());

                break;

            default :
                return Alternatif::all();
            break;
        }

    }

    function rekomendasi($kepentinganVariasiProduk,$kepentinganRata2Harga,$kepentinganKuantitasProduk,$kepentinganJarak,$lat,$longit){

        //total V
        $totKepentingan=$kepentinganVariasiProduk+$kepentinganRata2Harga+$kepentinganKuantitasProduk+$kepentinganJarak;
        // cari nilai w
        $w1=$kepentinganVariasiProduk/$totKepentingan;
        $w2=$kepentinganRata2Harga/$totKepentingan;
        $w3=$kepentinganKuantitasProduk/$totKepentingan;
        $w4=$kepentinganJarak/$totKepentingan;

          //ambil data toko dari database
        $data=AlternatifResource::collection(alternatif::select('id','kode_alternatif',
            'nama_alternatif','keterangan',
            'latitude',
            'longitude',
            //query untuk mengukur jarak
            DB::raw("ROUND(111.1111 * DEGREES(ACOS(COS(RADIANS($lat)) * COS(RADIANS(alternatif.latitude)) * COS(RADIANS($longit) - RADIANS(alternatif.longitude)) + SIN(RADIANS($lat)) * SIN(RADIANS(alternatif.latitude)))),2) as jarak"))
            //filter tooko yang berjarak <= 6 km
            ->where(DB::raw("ROUND(111.1111 * DEGREES(ACOS(COS(RADIANS($lat)) * COS(RADIANS(alternatif.latitude)) * COS(RADIANS($longit) - RADIANS(alternatif.longitude)) + SIN(RADIANS($lat)) * SIN(RADIANS(alternatif.latitude)))),2)"),'<=',$this->maxJarak)
            ->get());

        $newIdx=0;
        $result=array();

        foreach ($data as $k => $v) {
            //ambil data nilai bobot dari database
            $bobotAlternatif = BobotAlternatif::select('alternatif_id', 'kode_kriteria', 'nilai_bobot_alternatif',
                DB::raw("(select SUM(nilai_bobot_alternatif) from bobotalternatif WHERE alternatif_id=" . $v->id . " group by alternatif_id) as total_bobot_nilai"))->where('alternatif_id', $v->id)->get();

            if (count($bobotAlternatif) < 1) continue;

            $result[$newIdx]=array('nama_alternatif'=>$v->nama_alternatif,'keterangan'=>$v->keterangan);
            $s=1;

            foreach ($bobotAlternatif as $key => $val){
                if($val->kode_kriteria=='c4') {
                        $pangkat=pow($v->jarak,$w4);
                        $kodeKriteria="c4";
                        $nilai=$v->jarak;

                }
                elseif($val->kode_kriteria=='c1') {

                    $pangkat=pow($val->nilai_bobot_alternatif,$w1);
                    $kodeKriteria=$val->kode_kriteria;
                    $nilai=$val->nilai_bobot_alternatif;
                }
                elseif($val->kode_kriteria=='c2') {

                    $pangkat=pow($val->nilai_bobot_alternatif,$w2);
                    $kodeKriteria=$val->kode_kriteria;
                    $nilai=$val->nilai_bobot_alternatif;
                }
                elseif($val->kode_kriteria=='c3') {

                    $pangkat=pow($val->nilai_bobot_alternatif,$w3);
                    $kodeKriteria=$val->kode_kriteria;
                    $nilai=$val->nilai_bobot_alternatif;
                }


                $s=$s*$pangkat;


                $result[$newIdx]=array_merge($result[$newIdx],array("$kodeKriteria"=>$nilai,"w1"=>$w1,"w2"=>$w2,"w3"=>$w3,"w4"=>$w4,"s"=>$s));

            }
            $sx[]=$s;
           // $sTotal=array_sum($sx);
           // $result[$newIdx]=array_merge($result[$newIdx],array("sTotal"=>$sTotal));
           // $v=$result[$newIdx]['s']/$sTotal;
           // $result[$newIdx]=array_merge($result[$newIdx],array("v"=>$v));
            $newIdx++;
        }

        foreach ($result as $rk => $rv){
            $sTotal=array_sum($sx);
            $result[$rk]=array_merge($result[$rk],array("sTotal"=>$sTotal));
            $v=$rv['s']/$sTotal;
            $result[$rk]=array_merge($result[$rk],array("v"=>$v));
        }

        // return $result;
        usort($result,function ($i,$j){
            return $i['v'] < $j['v'];
        });


        // $enc = json_encode($result);
        // $dec = json_decode($enc, true);
         //echo json_encode($dec);
         //echo "<pre>";
        // print_r($result);
        // echo "</pre>";

        return $result;

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        alternatif::insert(
            [
                'kode_alternatif' => $request->kode_alternatif,
                'nama_alternatif' => $request->nama_alternatif,
                'keterangan'=>$request->keterangan,
                'latitude'=>$request->latitude,
                'longitude'=>$request->longitude,

            ]
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\alternatif  $alternatif
     * @return \Illuminate\Http\Response
     */
    public function show(alternatif $alternatif)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\alternatif  $alternatif
     * @return \Illuminate\Http\Response
     */
    public function edit(alternatif $alternatif)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\alternatif  $alternatif
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, alternatif $alternatif)
    {
        //
            alternatif::where('id',$request->id)->update(
             [
                'kode_alternatif' => $request->kode_alternatif,
                'nama_alternatif' => $request->nama_alternatif,
                'keterangan'=>$request->keterangan,
                'latitude'=>$request->latitude,
                'longitude'=>$request->longitude,
             ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\alternatif  $alternatif
     * @return \Illuminate\Http\Response
     */
    public function destroy(alternatif $alternatif)
    {
        alternatif::where('id',$alternatif->id)->delete();
        //
    }
}
