<?php

namespace App\Http\Controllers;

use App\alternatif;
use App\bobotAlternatif;
use Illuminate\Http\Request;
use App\Http\Resources\BobotAlternatif as BobotAlternatifResource;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
class BobotAlternatifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $act = Input::get('act');

        //
        switch ($act){
            case "filter" :

                break;

            default :
                $bobot=DB::table("alternatif")->select(
                    DB::raw("IFNULL((select nilai_bobot_alternatif from bobotalternatif where bobotalternatif.alternatif_id=alternatif.id and kode_kriteria='c1'),0) as c1"),
                    DB::raw("IFNULL((select nilai_bobot_alternatif from bobotalternatif  where bobotalternatif.alternatif_id=alternatif.id and kode_kriteria='c2'),0) as c2"),
                    DB::raw("IFNULL((select nilai_bobot_alternatif from bobotalternatif  where bobotalternatif.alternatif_id=alternatif.id and kode_kriteria='c3'),0) as c3"),
                    DB::raw("IFNULL((select nilai_bobot_alternatif from bobotalternatif  where bobotalternatif.alternatif_id=alternatif.id and kode_kriteria='c4'),0) as c4"),
                    DB::raw("IFNULL((select nilai_bobot_alternatif from bobotalternatif  where bobotalternatif.alternatif_id=alternatif.id and kode_kriteria='c4'),0) as c4"),
                    'id',
                    'nama_alternatif')->get();

                foreach ($bobot as $k =>$v){
                    $alt=alternatif::where('id',$v->id)->get();
                    $v->alternatif=$alt[0];
                }
                return $bobot;

/*
 *
 *                 return BobotAlternatifResource::collection(
                    alternatif::select(
                        DB::raw("(select nilai_bobot_alternatif from bobotalternatif where bobotalternatif.id=alternatif.id and kode_kriteria='c1') as c1"),
                        'nama_alternatif')
                        );
*/
                break;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        bobotAlternatif::updateOrInsert(
            [
                'alternatif_id' => $request->alternatif_id,
                'kode_kriteria'=>$request->kode_kriteria
            ],
            [
                'alternatif_id' => $request->alternatif_id,
                'kode_kriteria' => $request->kode_kriteria,
                'nilai_bobot_alternatif'=>$request->nilai_bobot_alternatif,

            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\bobotAlternatif  $bobotAlternatif
     * @return \Illuminate\Http\Response
     */
    public function show(bobotAlternatif $bobotAlternatif)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\bobotAlternatif  $bobotAlternatif
     * @return \Illuminate\Http\Response
     */
    public function edit(bobotAlternatif $bobotAlternatif)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\bobotAlternatif  $bobotAlternatif
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, bobotAlternatif $bobotAlternatif)
    {
        //
        bobotAlternatif::updateOrInsert(
            [
                'alternatif_id' => $request->alternatif_id,
                'kode_kriteria'=>$request->kode_kriteria
            ],
            [
                'alternatif_id' => $request->alternatif_id,
                'kode_kriteria' => $request->kode_kriteria,
                'nilai_bobot_alternatif'=>$request->nilai_bobot_alternatif,

            ]
        );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\bobotAlternatif  $bobotAlternatif
     * @return \Illuminate\Http\Response
     */
    public function destroy(bobotAlternatif $bobotAlternatif)
    {
        //
    }
}
