<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Alternatif extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'kode_alternatif'=>$this->kode_alternatif,
            'nama_alternatif'=>$this->nama_alternatif,
            'keterangan' => $this->keterangan,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'jarak'=>$this->jarak

            ];

    }

}
