<?php
/**
 * Created by PhpStorm.
 * User: Head of Programmer
 * Date: 12/06/2019
 * Time: 15.02
 */

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AlternatifCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
           $this->collection

        ];
    }
}