<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BobotAlternatif extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'kode_kriteria'=>$this->kode_kriteria,
            'nilai_bobot_alternatif'=>$this->nilai_bobot_alternatif

        ];
    }
}
